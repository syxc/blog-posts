Android安装程序报错:INSTALL_FAILED_DEXOPT
---------------------------

在Android3.0以前，dex限制了程序的最大方法数是65535，也就是说，如果你的系统是3.0以前的，同时你的程序的代码量太大，方法的数目超过了65535个，这个时候肯定会出现这个问题的。遇到这种情况的话，建议先使用3.0以后的设备进行开发和安装，屏蔽部分代码。

根本解决办法在这里，使用DexClassLoader动态加载类，同时打包的时候使用多个dex，详情见下面链接:

[Android打包多个dex并运行时动态加载类](http://android-developers.blogspot.hk/2011/07/custom-class-loading-in-dalvik.html)

Hack Dalvik VM解决Android 2.3 DEX/LinearAllocHdr超限

[http://viila.info/2014/04/android-2-3-dex-max-function-problem/](http://viila.info/2014/04/android-2-3-dex-max-function-problem/)

[https://github.com/viilaismonster/LinearAllocFix](https://github.com/viilaismonster/LinearAllocFix)

Android 插件化 动态升级

[http://www.trinea.cn/android/android-plugin/](http://www.trinea.cn/android/android-plugin/)
										